import Vue from 'vue'
import Router from 'vue-router'

import scrollBehavior from '~/.nuxt/router.scrollBehavior'
// const Index =()=>import('~/pages/PageIndex.vue')
// import Catalog from '~/pages/PageCatalog.vue'
const PageReviews = () => interopDefault(import('~/pages/reviews.vue'))
const PageIndex = () => interopDefault(import('~/pages/index.vue'))
const PageLoft = () => interopDefault(import('~/pages/loft.vue'))

function interopDefault(promise) {
    return promise.then((m) => m.default || m)
}

Vue.use(Router)

export function createRouter() {
    return new Router({
        mode: process.env.routerMode || 'history',
        base: process.env.NODE_ENV === 'production' ? '/chibbis' : '/',

        routes: [
            {
                path: '/',
                component: PageIndex,
            },
            {
                path: '/reviews',
                name: 'reviews',
                component: PageReviews,
            },
            {
                path: '/loft',
                name: 'loft',
                component: PageLoft,
            },
        ],
        scrollBehavior,
    })
}
