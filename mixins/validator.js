export default {
    data() {
        return {}
    },
    methods: {
        validatorFieldRules(name) {
            const obj = {}
            if (name === 'nameValidator') {
                obj.required = true
                obj.rules = [
                    (v) => !!v || 'Name is required',
                    (v) =>
                        (v && v.length <= 10) ||
                        'Name must be less than 10 characters',
                ]
            }
            if (name === 'genderValidator') {
                obj.required = true
                obj.rules = [(v) => !!v || 'Item is required']
            }

            if (name === 'agreeValidator') {
                obj.required = true
                obj.rules = [(v) => !!v || 'agree is required']
            }

            return obj
        },
    },
}
